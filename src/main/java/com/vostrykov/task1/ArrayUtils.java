package com.vostrykov.task1;

/**
 * @author <a href="mailto:it4alex@gmail.com">Olexiy Vostrykov</a>
 */

public class ArrayUtils {

    private static final int NOT_FOUND_ELEMENT = -1;

    private ArrayUtils() {
    }

    /**
     * Function searching a number in the array of numbers and return its index,
     * if number not found in array - return {@code NOT_FOUND_ELEMENT}
     * The array has descend order
     *
     * @param array  - descend ordered array
     * @param number - searching number
     * @return - index of found number in array, if number  not found - return {@code NOT_FOUND_ELEMENT}
     */
    public static int binarySearch(final int[] array, final int number) {
        if (array == null || array.length == 0) {
            return NOT_FOUND_ELEMENT;
        }
        int first = 0;
        int last = array.length - 1;
        int middle;
        if (number > array[first] || number < array[last])
            return NOT_FOUND_ELEMENT;
        while (first < last) {
            middle = first + ((last - first) / 2);
            if (number <= array[first] && number >= array[middle]) {
                last = middle;
            } else {
                first = middle + 1;
            }
        }
        return (array[first] == number) ? first : NOT_FOUND_ELEMENT;
    }
}
