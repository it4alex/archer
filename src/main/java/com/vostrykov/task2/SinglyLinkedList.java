package com.vostrykov.task2;

/**
 * Singly linked list with reverse function, that revert list in opposite order
 * Can not accept null values
 *
 * @author <a href="mailto:it4alex@gmail.com">Olexiy Vostrykov</a>
 */
public class SinglyLinkedList<E> {
    private static final int NOT_FOUND_INDEX = -1;
    private Entry<E> head;
    private int size = 0;

    private Entry<E> getHead() {
        return head;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }


    public boolean contains(E e) {
        return e != null && (indexOf(e) >= 0);
    }

    private Entry<E> findLast() {
        if (isEmpty()) return null;

        Entry<E> current = head;
        while (current.hasNext()) {
            current = current.next;
        }
        return current;
    }

    public boolean add(E e) {
        if (e == null) return false;
        try {
            Entry<E> current = new Entry<E>(e, null);
            if (isEmpty()) {
                head = current;
            } else {
                findLast().next = current;
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        size++;
        return true;
    }


    public boolean remove(E e) {
        if (isEmpty() || e == null) return false;
        Entry<E> current = getHead();
        Entry<E> prev = getHead();
        while (current != null) {
            if (e.equals(current.value)) {
                if (current == getHead()) {
                    head = current.next;
                } else {
                    prev.next = current.next;
                }
                size--;
                return true;
            }
            prev = current;
            current = current.next;
        }
        return false;
    }

    public void clear() {
        head = null;
        size = 0;
    }

    public E get(int index) {
        if ((index < 0) || (index > size() - 1 && !isEmpty())) return null;
        Entry<E> current = getHead();
        for (int i = 0; i < index; i++) {
            current = current.next;
        }
        return current.value;
    }

    public int indexOf(E e) {
        if (e == null || isEmpty()) return NOT_FOUND_INDEX;
        Entry<E> current = getHead();
        for (int i = 0; i < size(); i++) {
            if (e.equals(current.value)) return i;
            current = current.next;
        }
        return NOT_FOUND_INDEX;
    }


    public SinglyLinkedList<E> reverse() {
        Entry<E> newHead = null;
        while (head != null) {
            Entry<E> next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;

        }
        head = newHead;
        return this;
    }

    private static class Entry<E> {
        E value;
        Entry<E> next;

        Entry(E value, Entry<E> next) {
            this.value = value;
            this.next = next;
        }

        boolean hasNext() {
            return next != null;
        }
    }

}
