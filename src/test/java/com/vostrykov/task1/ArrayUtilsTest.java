package com.vostrykov.task1;

import org.junit.Test;

import static com.vostrykov.task1.ArrayUtils.binarySearch;
import static org.junit.Assert.assertEquals;

public class ArrayUtilsTest {

    private static final int EXPECTED_IF_NOT_FOUND = -1;
    private static final int[] DEFAULT_ARRAY = new int[]{100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0};

    @Test
    public void testBinarySearchArrayIsNull() throws Exception {
        assertEquals(EXPECTED_IF_NOT_FOUND, binarySearch(null, 5));
    }

    @Test
    public void testBinarySearchArrayIsEmpty() throws Exception {
        int[] array = new int[10];
        assertEquals(EXPECTED_IF_NOT_FOUND, binarySearch(array, 7));
    }

    @Test
    public void testBinarySearchNotInRangeValue() throws Exception {
        assertEquals(EXPECTED_IF_NOT_FOUND, binarySearch(DEFAULT_ARRAY, 500));
    }

    @Test
    public void testBinarySearchNotFoundValue() throws Exception {
        assertEquals(EXPECTED_IF_NOT_FOUND, binarySearch(DEFAULT_ARRAY, 5));
    }

    @Test
    public void testBinarySearchFoundValue() throws Exception {
        assertEquals(6, binarySearch(DEFAULT_ARRAY, 40));
    }

    @Test
    public void testBinarySearchFoundFirst() throws Exception {
        assertEquals(0, binarySearch(DEFAULT_ARRAY, 100));
    }

    @Test
    public void testBinarySearchFoundLast() throws Exception {
        assertEquals(10, binarySearch(DEFAULT_ARRAY, 0));
    }

    @Test
    public void testBinarySearchOneFromOneElement() throws Exception {
        int[] array = new int[]{10};
        assertEquals(0, binarySearch(array, 10));
        assertEquals(EXPECTED_IF_NOT_FOUND, binarySearch(array, 20));
    }

    @Test
    public void testBinarySearchFoundLastFromTwoElements() throws Exception {
        int[] array = new int[]{200, 100};
        assertEquals(1, binarySearch(array, 100));
    }

    @Test
    public void testBinarySearchFoundFirstFromTwoElements() throws Exception {
        int[] array = new int[]{200, 100};
        assertEquals(0, binarySearch(array, 200));
    }

    @Test
    public void testBinarySearchFoundLastFromThreeElements() throws Exception {
        int[] array = new int[]{300, 200, 100};
        assertEquals(2, binarySearch(array, 100));
    }

    @Test
    public void testBinarySearchFoundFirstFromThreeElements() throws Exception {
        int[] array = new int[]{300, 200, 100};
        assertEquals(0, binarySearch(array, 300));
    }

    @Test
    public void testBinarySearchFoundSecondFromThreeElements() throws Exception {
        int[] array = new int[]{300, 200, 100};
        assertEquals(1, binarySearch(array, 200));
    }

    @Test
    public void testBinarySearchFoundFirstFromThreeEqualElements() throws Exception {
        int[] array = new int[]{200, 200, 200};
        assertEquals(0, binarySearch(array, 200));
    }
}