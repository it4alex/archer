package com.vostrykov.task2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SinglyLinkedListTest {
    private SinglyLinkedList<Long> list;

    @Before
    public void setUp() {
        list = new SinglyLinkedList<Long>();
    }

    @Test
    public void testSize() {
        assertEquals(0, list.size());

        assertTrue(list.add(10L));
        assertEquals(1, list.size());

        assertTrue(list.add(20L));
        assertEquals(2, list.size());

        assertTrue(list.add(30L));
        assertEquals(3, list.size());

        assertFalse(list.add(null));
        assertEquals(3, list.size());

    }

    @Test
    public void testIsEmpty() {
        assertTrue(list.isEmpty());
        list.add(10L);
        assertFalse(list.isEmpty());
        list.remove(10L);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testContains() {
        list.add(10L);
        list.add(null);
        list.add(30L);
        assertTrue(list.contains(10L));
        assertFalse(list.contains(40L));
    }

    @Test
    public void testAdd() {
        assertTrue(list.add(10L));
        assertTrue(list.add(20L));
        assertTrue(list.add(30L));
        assertFalse(list.add(null));
        assertEquals(3, list.size());
    }

    @Test
    public void testRemoveMiddleElement() {
        list.add(10L);
        list.add(20L);
        list.add(30L);
        assertTrue(list.remove(20L));
        assertEquals(2, list.size());
        assertEquals(10L, (long) list.get(0));
        assertEquals(30L, (long) list.get(1));
    }

    @Test
    public void testRemoveFirstElement() {
        list.add(10L);
        list.add(20L);
        list.add(30L);
        assertTrue(list.remove(10L));
        assertEquals(2, list.size());
        assertEquals(20L, (long) list.get(0));
        assertEquals(30L, (long) list.get(1));
    }

    @Test
    public void testRemoveLastElement() {
        list.add(10L);
        list.add(20L);
        list.add(30L);
        assertTrue(list.remove(30L));
        assertEquals(2, list.size());
        assertEquals(10L, (long) list.get(0));
        assertEquals(20L, (long) list.get(1));
    }

    @Test
    public void testRemoveAll() {
        list.add(10L);
        list.add(20L);
        list.add(30L);
        assertTrue(list.remove(30L));
        assertTrue(list.remove(10L));
        assertTrue(list.remove(20L));
        assertEquals(0, list.size());
    }

    @Test
    public void testIndexOf() {
        list.add(10L);
        list.add(20L);
        list.add(30L);
        assertEquals(0, list.indexOf(10L));
        assertEquals(1, list.indexOf(20L));
        assertEquals(2, list.indexOf(30L));
        assertEquals(-1, list.indexOf(40L));

    }

    @Test
    public void testReverse() {
        list.add(10L);
        list.add(20L);
        list.add(30L);
        list.add(40L);

        assertEquals(0, list.indexOf(10L));
        assertEquals(1, list.indexOf(20L));
        assertEquals(2, list.indexOf(30L));
        assertEquals(3, list.indexOf(40L));

        list.reverse();

        assertEquals(0, list.indexOf(40L));
        assertEquals(1, list.indexOf(30L));
        assertEquals(2, list.indexOf(20L));
        assertEquals(3, list.indexOf(10L));

    }

    @Test
    public void testReverseEmpty() {
        list.reverse();
    }

    @Test
    public void testReverseOneElement() {
        list.add(10L);

        assertEquals(0, list.indexOf(10L));
        list.reverse();
        assertEquals(0, list.indexOf(10L));
    }

    @Test
    public void testClear() {
        list.add(10L);
        list.add(20L);
        assertFalse(list.isEmpty());
        list.clear();
        assertTrue(list.isEmpty());
    }


}